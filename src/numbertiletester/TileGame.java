package numbertiletester;

/**
 * Provides the methods need to play the game and display the game result; also
 * contains helper methods that handle tile placement and move making.
 * @author Kevin Baza
 */
public class TileGame{
   
    // instance variable declarations go here    
    private Hand playerOneHand;
    private Hand playerTwoHand;
    private Board gameBoard;
  
    private boolean isGameOver;

    private String winner = "";     //store the winner of the game
    private String finalBoard = "";  //store the final state the board reaches
    
    // Create a Tilegame object    
    // Initialize board, hand, tile objects.
    /**
     * Initializes the players' hands and the game board.
     */
    public TileGame(){
        playerOneHand = new Hand();
        playerTwoHand = new Hand();
        gameBoard = new Board();
        isGameOver = false;
    }
   
    // Play the game
    /**
     * Adds a tile to the board and begins the game by letting player one 
     * move first. Determines if the game has ended.
     */
    public void play(){

        // Add a tile to the board.
        gameBoard.addTile(0, new NumberTile());

        // First player one moves
        // Then player two moves
        // Check if either of their hands is empty. Game ends if at least one 
        // player's hand is empty.       
        do{
            makeMove(playerOneHand);
            makeMove(playerTwoHand);

            if(playerOneHand.getSize() == 0 && playerTwoHand.getSize() == 0){
                winner = "Tie";
                isGameOver = true;
            }
            else if(playerOneHand.getSize() == 0){
                isGameOver = true;
                winner = "Player One";
            }
            else if(playerTwoHand.getSize() == 0){
                isGameOver = true;
                winner = "Player Two";
            }
        }
        while(!isGameOver);
        // Display winner once game is over.
        System.out.println("Winner: " + winner + "\n\n");
    }

    // If the current tile fits in the board, returns the index at which 
    // it will be inserted.  If the tile does not fit, returns -1
    private int getIndexForFit(NumberTile tile){
        // Stores index locally.
        int indexForFit = 0;

        for(int i = 0; i < gameBoard.getSize() - 1; i ++){
            // Rotate tiles if needed.
            for(int j = 0; j <= 3; j ++){                
                // Check if the tile fits on the board.
                if((tile.getLeft() == gameBoard.getTile(i).getRight()) 
                    && (tile.getRight() == gameBoard.getTile(i + 1).getLeft())){

                    indexForFit = i + 1;
                    // Tile that fits found. Exit method.
                    return indexForFit;
                }
                else{
                    // Rotate tile and try again.
                    tile.rotate();
                }

            }

        }

        // Check the first tile.
        if(gameBoard.getSize() == 1){
            for(int i = 0; i <= 3; i ++){
                // Check that the tile can fit to the right of the 1st tile.
                if((tile.getLeft() == gameBoard.getTile(0).getRight())){
                    indexForFit = 1;
                    return indexForFit;
                }
                else{
                    tile.rotate();
                }
            }
        }
        
        // Check the last tile.
         if(gameBoard.getSize() > 1){

            for(int i = 0; i <= 3; i ++){
                // Check the right side of the last tile.
                if(tile.getLeft() == 
                    gameBoard.getTile(gameBoard.getSize() - 1).getRight()){    
                    indexForFit = gameBoard.getSize();
                    return indexForFit;
                }
                else{
                    tile.rotate();
                }
            }
        }
        return -1;
    }

   // Make a move from a hand. If a tile in the hand fits on the board
   // then remove it from the hand and place it in the board. The tile may
   // be rotated up to 3 times.  If no tile from the hand fits, then add
   // another tile to the hand  
    private void makeMove(Hand hand){
        int fitResult = 0;  // Index of fit.
        
        for(int i = 0; i < hand.getSize(); i ++){
            // Determine if the tile fits.
            fitResult = getIndexForFit(hand.get(i));
            
            // If a tile that fits is found, place it on board, remove 
            // from hand, and end player's turn.
            if(fitResult != -1){
                gameBoard.addTile(fitResult, hand.get(i));
                hand.removeTile(i);
                break;
            }                        
        }
        // Hand contains no tile that fits on the board. Add tile to hand.
        if(fitResult == -1){
            hand.addTile();
        }
    }
   
   // Get the results of the game as a humongous multi-line String containing
   // both starting hands, the final board, both final hands, and a message
   // indicating the winner 
   /**
    * Calls toString() methods of Hand and Board objects, then stores them in a
    * string.
    * @return String representation of the game results.
    */
    public String toString(){

        this.finalBoard = "Player One's final hand:\n "
            + "-------------------------------------\n"
            + playerOneHand.toString()
            + "\nPlayer Two's final hand:\n "       
            + "-------------------------------------\n"
            + playerTwoHand.toString()
            + "\nFinal Board:\n "
            + "-------------------------------------\n"
            + gameBoard.toString();
        return this.finalBoard;
    }
} // end of TileGame class
