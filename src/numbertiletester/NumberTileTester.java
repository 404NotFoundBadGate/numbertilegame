package numbertiletester;

/**
 *Tester class to run the program.
 *I affirm that this program is entirely my own work 
 *and none of it is the work of any other person.
 *@author Kevin Baza
 */
import java.util.Scanner;

public class NumberTileTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean playAgain = false;
        String usrInput = "";
        
        // Create a new game while the user wants to continue playing.
        do{
            Scanner scan = new Scanner(System.in);
            TileGame newGame = new TileGame();
        
            // Play the game.
            newGame.play();
            //Print out the player's final hands as well as the final board.
            System.out.println(newGame.toString());
            
            // Ask if user wants to play again.
            System.out.println("Would you like to play again? If yes, type "
                    + "\"yes\" or \"y.\" If not, type \"no\" or \"n.\\n");
            System.out.println("Choice: ");
            usrInput = scan.next();
            
            // Determine whether or not to play again.
            if(usrInput.toUpperCase().equals("YES") 
                    || usrInput.toUpperCase().equals("Y")){
                playAgain = true;
            }
            else if(usrInput.toUpperCase().equals("NO") 
                    || usrInput.toUpperCase().equals("N")){
                playAgain = false;
                System.out.println("Goodbye!");
            }
        }
        while(playAgain); 
    }
}
