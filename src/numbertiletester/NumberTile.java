package numbertiletester;

import java.util.ArrayList ;
import java.util.Random;
/**
 * A NumberTile is a tile with 4 random numbers ranging from 1 to 9 inclusive.
 */
public class NumberTile{
    private ArrayList<Integer> tile;
    private Random randGen = new Random();
    private static int TILE_NUMS = 4;   // Size of tile
    
    /**
     * Create a NumberTile object using 4 random ints in the range 1..9.
     */
    public NumberTile(){
        // Create a new number tile.
        tile = new ArrayList<Integer>();
        
        // Populate the tile with random numbers from 1 - 9.
        for(int i = 0; i < TILE_NUMS; i ++){
            tile.add(randGen.nextInt(10));
        }
    }
    
    /**
     * Rotate this tile 90 degrees clockwise.
     */
    public void rotate(){ 
        // Make a copy of the arraylist.
        ArrayList<Integer> tempTile = new ArrayList<Integer>(tile);
        
        // Move values one position to the right.
        for(int i = 0; i < TILE_NUMS; i ++){
            if(i == 3){
                tempTile.set(0, tile.get(i));
            }
            else{
                tempTile.set(i + 1, tile.get(i));
            }
        }
        // Copy the temp tile to the real tile.
        for(int i = 0; i < TILE_NUMS; i ++){
            tile.set(i, tempTile.get(i));
        }   
    }
    
    /**
     * Get the number on the left side of this tile
     * @return the number on the left side of this tile
     */
    public int getLeft(){
    	// DO NOT MODIFY THIS METHOD!
        // =========================
        return tile.get(0) ;
    }
    
    /**
     * Get the number on the right side of this tile
     * @return the number on the right side of this tile
     */
    public int getRight(){
        // DO NOT MODIFY THIS METHOD!
        // =========================
        return tile.get(2) ;
    }    
    
    /** 
     * Return a String representation of this tile in the form
     * 
     *    4
     * 5     7
     *    1
     * 
     * @return the tile as a multi-line String 
     */
    public String toString() {
        
        String tilePiece = "\n  " + tile.get(1) + "\n" + tile.get(0)
                    + "    " + tile.get(2) + "\n  " + tile.get(3);       
       
        return tilePiece;
    }    
} // end of NumberTile class